﻿using System;
using System.Collections.Generic;

namespace GroupProject
{

    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            var selection = 0;

            do
            {
                Console.Clear();
                Console.WriteLine("*******************************************************************");
                Console.WriteLine("*************               Menu                      *************");
                Console.WriteLine("*******************************************************************");
                Console.WriteLine("*****                                                         *****");
                Console.WriteLine("*****        Option: 1 (view Times table)                     *****");
                Console.WriteLine("*****        Option: 2 (Add and view numbers)                 *****");
                Console.WriteLine("*****        Option: 3 (months with 31 days )                 *****");
                Console.WriteLine("*****        Option: 4 (show name and sort by their months)   *****");
                Console.WriteLine("*****        Option: 5 (exit)                                 *****");
                Console.WriteLine("*****                                                         *****");
                Console.WriteLine("*******************************************************************");
                Console.WriteLine();
                Console.WriteLine("Choose a number:");
                var t = Console.ReadLine();
                bool checkInput = int.TryParse(t, out selection);

                if (checkInput)
                {
                    switch (selection)
                    {
                        case 1:
                            actions.program1();
                            break;
                        case 2:
                            actions.program2();
                            break;
                        case 3:
                            actions.program3();
                            break;
                        case 4:
                            actions.program4();
                            break;
                        case 5:
                            Console.Clear();
                            Console.WriteLine("Thanks for using the program");
                            break;
                        default:
                            Console.WriteLine($"You selected option {selection}");
                            Console.WriteLine("Press <Enter> to quit the program");
                            Console.ReadKey();
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("You didn't type in a number");
                    selection = 0;
                    Console.WriteLine("Press <Enter> to quit the program");
                    Console.ReadKey();
                }

                Console.WriteLine();

            } while (selection < 5);

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }

    }

    public class actions
    {
        public static void program1()
        {
            //Question no.1(Nikhil's code)
            int n, i;
            Console.WriteLine("Enter an integer to find multiplication table: ");
            while (!int.TryParse(Console.ReadLine(), out n))
            {
                Console.WriteLine("please put a number, not a character.");
            }
            for (i = 1; i <= 12; ++i)
            {
                Console.WriteLine(n + " * " + i + " = " + n * i);
            }
            Console.WriteLine("Press <Enter> to end the program");
            Console.ReadKey();
        }
        public static void program2()
        {
            //Question 2(bobby's code)

            Console.Clear();
            var count = 1;
            var input = "";
            var number = 0;
            double total = 0.00;
            List<int> numbers = new List<int>();

            while (count <= 5)
            {
                Console.WriteLine("Enter number " + count);
                input = Console.ReadLine();
                if (!int.TryParse(input, out number))
                {
                    Console.WriteLine("please put a number, not a character.");
                    count = count - 1;
                }
                else
                {
                    numbers.Add(number);
                    total += number;
                }
                count++;
            }
            Console.Clear();
            Console.WriteLine("The numbers you entered are:");
            foreach (int s in numbers)
            {
                Console.WriteLine(s);
            }

            Console.WriteLine($"The total of numbers are : {total}");

            Console.WriteLine("Press <Enter> to end the program");

            Console.ReadKey();
        }

        public static void program3()
        {
            //Question 3(Aman's Code)
            Dictionary<string, int> q3 = new Dictionary<string, int>();
            q3.Add("January", 31);
            q3.Add("February", 28);
            q3.Add("March", 31);
            q3.Add("April", 30);
            q3.Add("May", 31);
            q3.Add("June", 30);
            q3.Add("July", 31);
            q3.Add("August", 31);
            q3.Add("September", 30);
            q3.Add("October", 31);
            q3.Add("November", 30);
            q3.Add("December", 31);

            foreach (KeyValuePair<string, int> kvp in q3)
            {
                if (kvp.Value == 31)
                {
                    Console.WriteLine("Month = {0}, Days = {1}", kvp.Key, kvp.Value);
                }
            }
            Console.WriteLine("");
            Console.WriteLine("Press <Enter> to end the program");
            Console.ReadKey();
        }
        public static void program4()
        {
            //Question 4(Karan's code)
            Console.WriteLine("The following are name and month they are born in : ");
            List<monthsandnames> list = new List<monthsandnames>();
            list.Add(new monthsandnames("bob", "October", 10));
            list.Add(new monthsandnames("bret", "January", 1));
            list.Add(new monthsandnames("mary", "March", 3));
            list.Add(new monthsandnames("bill", "April", 4));
            list.Add(new monthsandnames("sid", "May", 5));
            list.Add(new monthsandnames("john", "June", 6));
            list.Add(new monthsandnames("jim", "July", 7));
            list.Add(new monthsandnames("rick", "August", 8));
            list.Add(new monthsandnames("richard", "september", 9));
            list.Add(new monthsandnames("sam", "February", 2));

            list.Sort();

            foreach (monthsandnames a in list)
            {
                Console.WriteLine(a.name + "-" + a.month);
            }
            Console.WriteLine("");
            Console.WriteLine("Press <Enter> to end the program");
            Console.ReadKey();
        }
    }

}


