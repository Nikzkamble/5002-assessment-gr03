using System;

namespace GroupProject
{
   class monthsandnames : IComparable<monthsandnames>
{
       public  string name {get; set; }
       public  string month {get; set; }

         public  int i {get; set; }

       public monthsandnames(string _name,string _month, int _i)
       {
           name = _name;
           month = _month;
           i = _i;
       }

        public int CompareTo(monthsandnames other)
    {
        return this.i.CompareTo(other.i);
    }
}
}
